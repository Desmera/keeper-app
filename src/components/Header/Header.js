import React from "react";

import {ThemeProvider} from "styled-components";
import { GlobalStyles } from "../Themes/GlobalStyles";
import { lightTheme, darkTheme } from "../Themes/Themes"
import {useDarkMode} from "../userDarkMode/useDarkMode";
import Toggle from "../Toggler/Toggler";




const Header = () => {

  const [theme, themeToggler, mountedComponent] = useDarkMode();
  const themeMode = theme === 'light' ? lightTheme : darkTheme;

  if(!mountedComponent) return <div/>

  return (

    <ThemeProvider theme={themeMode}>

      <>

      <GlobalStyles />
        <header>
          <div className="keeperLogoText">
            <h1>Keeper</h1>
          </div>
          <div className="btnToggler">
            <Toggle theme={theme} toggleTheme={themeToggler} />
          </div>
        </header>
      </>

     </ThemeProvider>
  );
};

export default Header;
