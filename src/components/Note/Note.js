
import React, { useState } from "react";
import { TwitterPicker } from 'react-color';
import {Image} from "react-bootstrap";


export default function Note(props) {

  // in the beginig the property is always false, because I'm not change it in the moment
  const [editNote, setEditNote] = useState(false);

  // state 2
  const [newNote, setNewNote] = useState({title:"", content:""});

  // state 3 ->  where to save the note info typed
  const [noteInput, setNoteInput] = useState({title:"", content:""});
  
  // setting color 
  const [color, setColor] = useState("");

  
  const handleDelete = () => {
    props.deleteNote(props.id);
  };

  const handleEdit = () => {
    // change the status value of editNote
    setEditNote(true);
  };

  // create  new function to controll where I save things
  const handleChangeTitle = (event) => {
    setNoteInput(
      (prevValue) => ({
        ...prevValue,
        title:event.target.value
      })
    );
  }
  
  const handleChangeContent = (event) => {
    setNoteInput(
      // pass a function to receive previous state as a parameter
      (prevValue) => ({
        // call the preserve the previous value
        ...prevValue,
        // it's like a property that I'm adding
        content:event.target.value
      })
    );
  }

  // function to controll save
  const hadleSave = () => {
    // saves title and content on state 2 instead of state 3
    setNewNote({title: noteInput.title, content: noteInput.content});

    // removes the inputs and save btn after click on save
    setEditNote(false);
    
  }

  const handleSaveColor = (color) => {
    setColor(color.hex);
  };


  return (
    <div className='note' style={{backgroundColor: color}}>
      <h1>{editNote || newNote.title !== "" ? newNote.title : props.title}</h1>
      <p>{editNote || newNote.content !== "" ? newNote.content : props.content}</p>
      
      {editNote ? 
        <div>
          <input onChange={handleChangeTitle} value={noteInput.title} className="inputChangeContent" />
          <input onChange={handleChangeContent} value={noteInput.content} className="inputChangeContent" />

          <TwitterPicker className="colorPickerSize" color={color} onChangeComplete={ handleSaveColor } />

          <button onClick={hadleSave} className="btnNote">
            <p><Image className="btnImg" src="https://cdn-icons.flaticon.com/png/512/3363/premium/3363856.png?token=exp=1635004392~hmac=b99b4baca1a6b0482a7fffc520717307"
            width={"16px"} height={"16px"} rounded/></p>
          </button>
        </div> : null
      }
      
      <button onClick={handleDelete} className="btnNote">
        <p><Image className="btnImg" src="https://cdn-icons.flaticon.com/png/512/3363/premium/3363974.png?token=exp=1634900838~hmac=92ae981c23b8ea5452d20303adf5f0f0"
        width={"16px"} height={"16px"} rounded/></p>
      </button>

      <button onClick={handleEdit} className="btnNote">
        <p><Image className="btnImg" src="https://cdn-icons.flaticon.com/png/512/3364/premium/3364173.png?token=exp=1634900838~hmac=f90226bcc9657ff2da6dccf9f01c6722"
        width={"16px"} height={"16px"} rounded/></p>
      </button>
    </div>
  );
};