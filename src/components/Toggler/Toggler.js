import React from 'react';
import { func, string } from 'prop-types';
import styled from "styled-components";
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';


const Button = styled.button `
  
  background-color: #add0e7;
  border: 1px solid ${({ theme }) => theme.toggleBorder};
  border-radius: 30px;
  cursor: pointer;
  font-size: 0.5rem;
  justify-content: space-between;
  margin: 0 auto;
  overflow: hidden;
  padding: 0.5rem;
  position: relative;
  width: 3rem;
  height: 3rem;
  outline: none;


`;

const Toggle = ({theme,  toggleTheme }) => {
    return (
      <Button onClick={toggleTheme}>
        {theme === "light" ? <LightModeIcon /> : <DarkModeIcon />}
      </Button>
    );
};

Toggle.propTypes = {
    theme: string.isRequired,
    toggleTheme: func.isRequired,
}


export default Toggle;
